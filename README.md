# CPE 409 Projects #

### About ###
This repository contains all work done for CPE 409 - Developing With Cloud Services at Cal Poly (Winter 2015 Quarter).
Instructor: Jay Schultz

### Folder Structure ###
The repository is broken down by each project and its required files.

### Copying / Use of Work ###
All work in this repository is owned by Sean Marpo, and should not be used for other's coursework. Feel free to reference the content, but copy at your own risk! You may be at risk for being caught cheating!

### Contact ###
smarpo@calpoly.edu