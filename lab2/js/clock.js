function getTime() {
    var time = new Date();
    var hrs = time.getHours();
    var mins = time.getMinutes();
    var secs = time.getSeconds();
    document.getElementById("clock").innerHTML = hrs + ":" + mins + ":" + secs ;
    setTimeout(getTime, 1000);
}
