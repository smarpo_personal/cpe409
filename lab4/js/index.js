$( function() {
    getTemp();
});

function getTime() {
    var time = new Date();
    var hrs = time.getHours();
    var mins = time.getMinutes();
    var secs = time.getSeconds();
    document.getElementById("clock").innerHTML = hrs + ":" + mins + ":" + secs ;
    setTimeout(getTime, 1000);
}

function getTemp() {
    $.getJSON( "https://api.forecast.io/forecast/a3e709ac444c5abbb8d45c02bb6f6532/35.300399,-120.662362?callback=?",
    function(result) {
        $( "#forecastLabel" ).prepend("<p>" + result.daily.summary + "</p><br>");
        $( "#forecastIcon" ).attr("src", "img/" + result.currently.icon + ".png");
        if (result.currently.temperature < 60) {
            $( "body" ).addClass("cold");
        }
        else if (result.currently.temperature < 69) {
            $( "body" ).addClass("chilly");
        }
        else if (result.currently.temperature < 79) {
            $( "body" ).addClass("nice");
        }
        else if (result.currently.temperature < 89) {
            $( "body" ).addClass("warm");
        }
        else {
            $( "body" ).addClass("hot");
        }
    });
}
