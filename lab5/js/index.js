$( function() {
    getTemp();
    getAllAlarms();
    getTime();
});

function getAllAlarms() {
    Parse.initialize("NhaS31VaH8fl91WyfvlDOjo1iifJceeNFbYkqkQT", "mi6OcWMqrZXBtq7SSb15gq9VWbCDTTjvIcEu0uac");
    var AlarmObject = Parse.Object.extend("Alarm");
    var query = new Parse.Query(AlarmObject);
    query.find({
        success: function(results) {
            for (var i = 0; i < results.length; i++) {
                insertAlarm(results[i].get('time'), results[i].get('alarmName'));
            }
        }
    });
}

function showAlarmPopup() {
    $("#mask").removeClass("hide");
    $("#popup").removeClass("hide");
}

function hideAlarmPopup() {
    $("#popup").addClass("hide");
    $("#mask").addClass("hide");
}

function insertAlarm(t, name) {
    var newDiv = $("<div>");
    newDiv.addClass("flexable");
    newDiv.append("<div class='name'>" + name + "</div><div class='time'>"
        + t + "</div><div class='delete' onclick='deleteAlarm()'>X</div>");
    $("#alarms").append(newDiv);
}

function deleteAlarm() {
    var alarmObj = event.target;
    var alarmName = alarmObj.parentNode.childNodes[0].innerHTML;

    var AlarmObject = Parse.Object.extend("Alarm");
    var query = new Parse.Query(AlarmObject);
    query.equalTo("alarmName", alarmName);
    query.find({
        success: function(results) {
            for (var i = 0; i < results.length; i++) {
                results[i].destroy({
                  success: function(myObject) {
                    // The object was deleted from the Parse Cloud.
                    alert("Alarm was deleted!");
                  },
                  error: function(myObject, error) {
                    // The delete failed.
                    // error is a Parse.Error with an error code and message.
                    alert("An error occurred with Parse!");
                  }
                });
            }
        }
    });
    alarmObj.parentNode.remove();
}

function addAlarm() {
    var hrs, mins, ampm, name, time;
    hrs = $("#hours option:selected").text();
    mins = $("#mins option:selected").text();
    ampm = $("#ampm option:selected").text();
    name = $("#alarmName").val();
    time = hrs + ":" + mins + " " + ampm;

    var AlarmObject = Parse.Object.extend("Alarm");
    var alarmObject = new AlarmObject();
    alarmObject.save({"time": time,"alarmName": name}, {
      success: function(object) {
            insertAlarm(time, name);
            hideAlarmPopup();
        }
    });
}

function getTime() {
    var time = new Date();
    var hrs = time.getHours();
    var mins = time.getMinutes();
    var secs = time.getSeconds();
    document.getElementById("clock").innerHTML = hrs + ":" + mins + ":" + secs ;
    setTimeout(getTime, 1000);
}

function getTemp() {
    $.getJSON( "https://api.forecast.io/forecast/a3e709ac444c5abbb8d45c02bb6f6532/35.300399,-120.662362?callback=?",
        function(result) {
            $( "#forecastLabel" ).prepend("<p>" + result.daily.summary + "</p><br>");
            $( "#forecastIcon" ).attr("src", "img/" + result.currently.icon + ".png");
            if (result.currently.temperature < 60) {
                $( "body" ).addClass("cold");
            }
            else if (result.currently.temperature < 69) {
                $( "body" ).addClass("chilly");
            }
            else if (result.currently.temperature < 79) {
                $( "body" ).addClass("nice");
            }
            else if (result.currently.temperature < 89) {
                $( "body" ).addClass("warm");
            }
            else {
                $( "body" ).addClass("hot");
            }
    });
}
