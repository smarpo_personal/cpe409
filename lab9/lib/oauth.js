// var popup;
// var timer;
var json = {client_id: "", type: "", callback: null};

$(function() {
    init(json);
    checkTokenExists();
});

function init(blob) {
    blob.client_id = "2a408d60954a61f";
    blob.type = "token";
    blob.callback = requestAccountInfo;
}

function login() {
    var imgurUrl = "https://api.imgur.com/oauth2/authorize?client_id=" + json.client_id + "&response_type=" + json.type;

    window.open(imgurUrl, 'imgur','left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
    //timer = setInterval(checkChild, 500);
}

// function checkChild() {
//     if (popup.closed) {
//         clearInterval(timer);
//         requestAccountInfo();
//     }
// }

function checkTokenExists() {
    if (localStorage.getItem("oauth_token") != null) {
        requestAccountInfo();
    }
}

function requestAccountInfo() {
    var accessToken = localStorage.getItem('oauth_token');

    $.ajax({
        beforeSend: function (request)
        {
            request.setRequestHeader("Authorization", "Bearer " + accessToken);
        },
        url: "https://api.imgur.com/3/account/me",
        success: function(msg) {
            $("#headertext").html("You are logged into Imgur!<br>Your username: " + msg.data.url);
            $(".btn").attr("style", "display:none;");
            alert(msg.data.url);
        },
        error: function() {
            localStorage.removeItem('oauth_token');
            console.log("Auto sign-in failed! Your token has expired!");
        }
    });
}
