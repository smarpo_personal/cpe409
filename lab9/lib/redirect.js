$(function() {
    redirect_init();
});

function redirect_init() {
    var hashUrl = window.location.hash;
    var split = hashUrl.split("&");
    var accessToken = split[0].replace("#access_token=", "");

    localStorage.setItem('oauth_token', accessToken);
    window.opener.json.callback();
    window.close();
}
